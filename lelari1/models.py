from django.db import models
from django.conf import settings


class Book(models.Model):
    name = models.CharField(max_length=255)
    post_date = models.IntegerField()
    poster_url = models.URLField(max_length=200, null=True)

    def __str__(self):
        return f'{self.name} ({self.post_date})'


class Review(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    likes = models.IntegerField(default=0)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)

    def __str__(self):
        return f'"{self.text}" - {self.author.username}'


class Comment(models.Model):
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete = models.CASCADE,
    )

    text = models.CharField(max_length=255)
    publi_date = models.DateTimeField()
    #book = models.ForeignKey(Book, on_delete=models.CASCADE)

    def __str__(self):
        return f'"{self.text}" ({self.publi_date}) - {self.author.username}'

class Category(models.Model):
    name = models.CharField(max_length=255)
    descr = models.CharField(max_length=255)
    def __str__(self):
        return f'"{self.descr}" {self.name}'

