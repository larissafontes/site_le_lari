book_data = [{
    "id":
    "1",
    "name":
    "As Mentiras de Locke Lamora",
    "release_year":
    "2006",
    "poster_url":
    "https://literaturaempauta.com.br/wp-content/uploads/2016/09/mentiras-de-locke-lamora-capa-679x1024.jpg"
}, {
    "id":
    "2",
    "name":
    "A Paciente Silenciosa",
    "release_year":
    "2019",
    "poster_url":
    "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRMjPCIsgWA9JCaPYdlCALi_hX59ErHcMa8qEHFBANQXW75DnWl"
}, {
    "id":
    "3",
    "name":
    "Trono de Vidro",
    "release_year":
    "2012",
    "poster_url":
    "https://m.media-amazon.com/images/I/41NhERQs9DL._SY344_BO1,204,203,200_QL70_ML2_.jpg"
}, {
    "id":
    "4",
    "name":
    "Trono de Vidro",
    "release_year":
    "2007",
    "poster_url":
    "https://books.google.com.br/books/content?id=7-9zwIWkAlEC&hl=pt-BR&pg=PP1&img=1&zoom=3&sig=ACfU3U3XNk5Mz4g5rdqw0wGgQ-NW6KGtcA&w=1280"
}, 

]