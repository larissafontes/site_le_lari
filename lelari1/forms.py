from django.forms import ModelForm
from .models import Book, Comment, Category


class BookForm(ModelForm):
    class Meta:
        model = Book
        fields = [
            'name',
            'post_date',
            'poster_url',
        ]
        labels = {
            'name': 'Título',
            'post_date': 'Data de Lançamento',
            'poster_url': 'URL do Poster',
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            'author',
            'text',
            'publi_date',
        ]

        labels = {
            'author': 'Usuário',
            'text': 'Comentário',
            'publi_date': 'Data da postagem'
        }

class CategoryForm(ModelForm):
    class Meta:
        model = Category
        fields = [
            'name',
            'descr',
        ]
        labels = {
            'name': 'Título',
            'descr': 'Descrição'
        }