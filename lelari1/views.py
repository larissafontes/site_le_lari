from django.shortcuts import render, get_object_or_404

from django.http import HttpResponse
from django.http import HttpResponseRedirect
from .temp_data import book_data
from django.urls import reverse
from .models import Book, Comment, Category
from django.views import generic
from django.urls import reverse_lazy
from .forms import BookForm, CommentForm, CategoryForm

class BookListView(generic.ListView):
    model = Book
    template_name = 'lelari1/index.html'

class BookDetailView(generic.DetailView):
    model = Book
    template_name = 'lelari1/detail.html'

# class BookDeleteView(generic.DeleteView):
    #model = Book
    #template_name = 'lelari1/delete.html'
    #sucess_url = reverse_lazy ('index:lelari1')

#class BookUpdateView(generic.UpdateView):
    #model = Book
    #template_name = 'lelari1/update.html'
    #fields = ['name']
    #def get_absolute_url(self):
        #return  reverse('lelari1:detail', args=(book.id, ))


def detail_book(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    context = {'book': book}
    return render(request, 'lelari1/detail.html', context)

def list_lelari1(request):
    book_list = Book.objects.all()
    context = {"book_list": book_list}
    return render(request, 'lelari1/index.html', context)


def search_lelari1(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        book_list = Book.objects.filter(name__icontains=search_term)
        context = {"book_list": book_list}
    return render(request, 'lelari1/search.html', context)

def create_book(request):
    if request.method == 'POST':
        form = BookForm(request.POST)
        if form.is_valid():
            book_name = request.POST['name']
            book_post_date = request.POST['post_date']
            book_poster_url = request.POST['poster_url']
            book = Book(name= book_name, post_date = book_post_date, 
                        poster_url= book_poster_url)
            book.save()
            return HttpResponseRedirect(
                reverse('lelari1:detail', args=(book.id,) ))
    else:
        form = BookForm()
        context = {'form': form}
        return render(request, 'lelari1/create.html', context)

def update_book(request, book_id):
    book = get_object_or_404(Book, pk=book_id)

    if request.method == "POST":
        if form.is_valid():
            book.name = request.POST['name']
            book.post_date = request.POST['post_date']
            book_poster_url = request.POST['poster_url']
            book.save()
            return HttpResponseRedirect(
                reverse('lelari1:detail', args=(book.id, )))
    else:
        form= BookForm(
            initial ={
                'name': book.name,
                'post_date': book.post_date,
                'poster_url': book.poster_url
            }
        )

    context = {'book': book, 'form': form}
    return render(request, 'lelari1/update.html', context)


def delete_book(request, book_id):
    book = get_object_or_404(Book, pk=book_id)

    if request.method == "POST":
        book.delete()
        return HttpResponseRedirect(reverse('lelari1:index'))

    context = {'book': book}
    return render(request, 'lelari1/delete.html', context)

def create_comment(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_text = form.cleaned_data['text']
            comment_publi_date = form.cleaned_data['publi_date']
            comment = Comment(author=comment_author,
                            text=comment_text,
                            publi_date = comment_publi_date
                            #book=book
                            )
            comment.save()
            return HttpResponseRedirect(
                reverse('lelari1:detail', args=(book_id, )))
    else:
        form = CommentForm()
    context = {'form': form, 'book': book}
    return render(request, 'lelari1/comment.html', context)

class CategoryListView(generic.ListView):
    model = Category
    template_name = 'lelari1/category.html'

class CategoryCreateView(generic.CreateView):
    model = Category
    templeta_name = 'lelari1/category_list.html'
    fields = ['nome', 'descr']
    sucess_url = reverse_lazy('lelari1:category')