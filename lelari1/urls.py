from django.urls import path

from . import views

app_name = 'lelari1'
urlpatterns = [
    path('', views.BookListView.as_view(), name='index'),
    path('search/', views.search_lelari1, name = 'search'),
    path('create/', views.create_book, name='create'),
    path('<int:pk>/', views.BookDetailView.as_view(), name='detail'),
    path('update/<int:book_id>/', views.update_book, name='update'),
    path('delete/<int:book_id>/', views.delete_book, name='delete'),
    path('<int:book_id>/comment/', views.create_comment, name='comment'),
    path('category/', views.CategoryListView.as_view(), name = 'category'),
    path('category/list', views.CategoryCreateView.as_view(), name ='category-list' )

    
    #path('update/<int:pk>/', views.BookUpdateView.as_view(), name='update'),
    #path('delete/<int:pk>/', views.BookDeleteView.as_view(sucess_url="index:lelari1"), name='delete'),
]